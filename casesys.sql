create database casesys;

use casesys;

create table tb_user
(
    id         int auto_increment comment '用户id'
        primary key,
    accounts   varchar(30)   not null comment '账号',
    passwords  varchar(20)   not null comment '密码',
    user_name  varchar(20)   not null comment '使用人',
    department varchar(50)   not null comment '所属部门',
    user_Id    varchar(18)   not null comment '身份证号',
    police_num varchar(6)    not null comment '警号',
    acc_type   int default 0 not null comment '账号类型：用户、管理员、超管',
    is_delete  int default 0 not null comment '逻辑删除（0:没删,1:删)',
    constraint tb_user_accounts_uindex
        unique (accounts)
);

create table tb_case_info
(
    id                 int(20) auto_increment comment '序号',
    case_name          varchar(50)        not null comment '案件名称',
    case_id            varchar(50)        not null comment '案件编号',
    case_begin         timestamp          not null comment '立案时间',
    accept_org         varchar(50)        not null comment '受案单位',
    accept_staff       varchar(20)        not null comment '办理人员',
    suspects_name      varchar(20)        not null comment '嫌犯姓名',
    suspects_sex       varchar(2)         not null comment '嫌犯性别',
    suspects_id        varchar(18)        not null comment '嫌犯身份证号',
    case_area          varchar(100)       not null comment '案发地区',
    case_brief         varchar(255)       not null comment '简要案情',
    case_charges       varchar(50)        not null comment '涉案罪名',
    record             smallint default 0 not null comment '有无前科(0:无前科 1:有前科)',
    tk_coercive_time   timestamp          null comment '采取强制措施时间',
    tk_coercive_type   varchar(20)        null comment '采取强制措施种类',
    tk_coercive_basis  varchar(255)       null comment '采取强制措施依据',
    cg_coercive_time   timestamp          null comment '变更强制措施时间',
    cg_coercive_type   varchar(20)        null comment '变更强制措施种类',
    cg_coercive_basis  varchar(255)       null comment '变更强制措施依据',
    deadline_warn      timestamp          not null comment '时间期限预警',
    case_prg           varchar(20)        not null comment '办案进度',
    case_op            varchar(50)        null comment '案件审查意见',
    note               varchar(50)        null comment '备注',
    user_number        int                not null comment '用户id',
    is_delete          int      default 0 not null comment '0:表示没删,1:表示删除',
    cg_coercive_times  timestamp          null comment '变更强制措施时间',
    cg_coercive_style  varchar(20)        null comment '变更强制措施种类',
    cg_coercive_timing timestamp          null comment '变更强制措施时间',
    cg_coercive_genre  varchar(20)        null comment '变更强制措施种类',
    primary key (id, case_id),
    constraint case_info_case_id_uindex
        unique (case_id),
    constraint case_info_id_uindex
        unique (id),
    constraint tb_case_info_ibfk_1
        foreign key (user_number) references tb_user (id)
);

create index user_number
    on tb_case_info (user_number);

create table tb_check
(
    id                  int auto_increment comment '审查id'
        primary key,
    check_target        varchar(600) null comment '主办民警工作目标、计划',
    check_term          timestamp    null comment '审结期限',
    responsible_opinion varchar(200) null comment '大队负责人意见',
    leader_opinion      varchar(200) null comment '支队领导意见',
    case_id             int          not null comment '案件id',
    constraint tb_check_ibfk_1
        foreign key (case_id) references tb_case_info (id)
);

create index case_id
    on tb_check (case_id);

create table tb_evid_info
(
    id            int auto_increment comment '序号',
    case_id       varchar(50)                         not null comment '案件编号',
    evid_type     varchar(20)                         not null comment '证据类型',
    upload_time   timestamp default CURRENT_TIMESTAMP not null comment '上传时间',
    file_name_ser varchar(255)                        null comment '服务端文件名字',
    note          varchar(255)                        null comment '文本',
    file_name_cli varchar(255)                        null comment '客户端文件名字',
    update_time   timestamp                           null comment '证据修改时间',
    constraint tb_evid_info_id_uindex
        unique (id),
    constraint tb_evid_info_tb_case_info_case_id_fk
        foreign key (case_id) references tb_case_info (case_id)
            on update cascade on delete cascade
);

alter table tb_evid_info
    add primary key (id);

create table tb_writ
(
    id            int auto_increment comment '文件id'
        primary key,
    writ_name_ser varchar(255) null comment '服务器端文件名称',
    writ_name_cli varchar(255) null comment '客户端文件名称',
    user_id       int          null comment '上传者的id',
    writ_type     varchar(50)  not null comment '文件类型',
    constraint tb_writ_ibfk_1
        foreign key (user_id) references tb_user (id)
);

create index user_id
    on tb_writ (user_id);
