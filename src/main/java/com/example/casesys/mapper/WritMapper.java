package com.example.casesys.mapper;

import com.example.casesys.entity.Writ;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author kongke
* @description 针对表【tb_writ(文书)】的数据库操作Mapper
* @createDate 2023-10-13 20:05:46
* @Entity com.example.casesys.entity.Writ
*/
@Mapper
public interface WritMapper extends BaseMapper<Writ> {

}




