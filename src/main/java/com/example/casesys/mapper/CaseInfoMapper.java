package com.example.casesys.mapper;

import com.example.casesys.entity.CaseInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.commons.lang3.builder.ToStringExclude;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 案件信息表 Mapper 接口
 * </p>
 *
 * @author hongren
 * @since 2023-09-11
 */
@Mapper
public interface CaseInfoMapper extends BaseMapper<CaseInfo> {
    @Select("select distinct case_charges from tb_case_info")
    List<String> selectCaseCharges();

    @Select("select distinct accept_org from tb_case_info")
    List<String> selectAcceptOrg();

    @Select("select distinct case_prg from tb_case_info")
    List<String> selectCasePrg();

}
