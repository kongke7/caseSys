package com.example.casesys.mapper;

import com.example.casesys.entity.EvidInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 证据信息表 Mapper 接口
 * </p>
 *
 * @author hongren
 * @since 2023-09-11
 */
@Mapper
public interface EvidInfoMapper extends BaseMapper<EvidInfo> {

}
