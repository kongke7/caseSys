package com.example.casesys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.casesys.entity.Check;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 审核信息表 Mapper 接口
 * </p>
 *
 * @author hongren
 * @since 2023-09-11
 */
@Mapper
public interface CheckMapper extends BaseMapper<Check> {
}
