package com.example.casesys;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
@MapperScan("com.example.casesys.mapper")
public class CaseSysApplication extends SpringBootServletInitializer {

    //打包时配置
//    @Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
//        return builder.sources(CaseSysApplication.class);
//    }

    public static void main(String[] args) {
        SpringApplication.run(CaseSysApplication.class, args);
    }

}

