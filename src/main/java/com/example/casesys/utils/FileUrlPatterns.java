package com.example.casesys.utils;


import java.io.File;

public class FileUrlPatterns {

    /**
     * 系统文件中路径的斜杠
     * win中为 \
     * linux中为 /
     */
    public static final String FILE_SEPARATOR = File.separator;

    /**
     * 数据库路径转移字符影响，设置两个路径常量
     */
    public static final String FILE_URL = "D:"+ FILE_SEPARATOR +"file"+FILE_SEPARATOR;

    public static final String FILE_URL_LINUX = "/"+"caseFile"+FILE_SEPARATOR;

    public static final String FILE_TO_DB_URL = "D:/file/";

    public static final String cutUrl = "/";
}
