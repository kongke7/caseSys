package com.example.casesys.utils;

public abstract class RegexPatterns {
    /**
     * 用户编号
     */
    public static final String NUMBER_REGEX = "^[0-9a-zA-Z]{8}$";

    /**
     * 身份证号码验证
     */
    public static final String ID_NUMBER = "^(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)$";

    /**
     * 密码格式验证
     */
    public static final String PASSWORD = "^[a-zA-Z0-9]{8,}$";
}
