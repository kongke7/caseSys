package com.example.casesys.utils;
import com.example.casesys.dto.CaseRequestParams;
import com.example.casesys.entity.CaseInfo;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class FilterUtil {


    public static List<CaseInfo> FilterCase(List<CaseInfo> caseInfosList, CaseRequestParams message) {
        if(caseInfosList != null && !caseInfosList.isEmpty()) {
            if(message.isEmpty()){
                return caseInfosList;
            }
            List<CaseInfo> caseInfos1 = caseInfosList;
            if (message.getCaseCharges() != null && !message.getCaseCharges().equals("")) {
                caseInfos1 = filterCaseByCharges(caseInfosList, message.getCaseCharges());
            }
            if (message.getAcceptOrg() != null && !message.getAcceptOrg().equals("")) {
                caseInfos1 = filterCaseByAcceptOrg(caseInfos1, message.getAcceptOrg());
            }
            if (message.getCasePrg() != null && !message.getCasePrg().equals("")) {
                caseInfos1 = filterCaseByCasePrg(caseInfos1, message.getCasePrg());
            }
            if (message.getCaseBegin() != null && message.getCaseEnd() != null) {
                caseInfos1 = filterCaseByCaseBegin(caseInfos1, message.getCaseBegin(), message.getCaseEnd());
            }
            return caseInfos1;
        }
        return null;
    }
    public static List<CaseInfo> filterCaseByCharges(List<CaseInfo> caseInfoList, String caseCharges) {
        List<CaseInfo> filteredProducts = new ArrayList<>();
        if(caseInfoList != null && !caseInfoList.isEmpty()) {
            for (CaseInfo caseInfo : caseInfoList) {
                if (caseInfo.getCaseCharges().equals(caseCharges)) {
                    filteredProducts.add(caseInfo);
                }
            }
            return filteredProducts;
        }else {
            return null;
        }
    }

    public static List<CaseInfo> filterCaseByAcceptOrg(List<CaseInfo> caseInfos, String acceptOrg) {
        List<CaseInfo> filteredProducts = new ArrayList<>();
        if(caseInfos != null && !caseInfos.isEmpty()) {
            for (CaseInfo caseInfo : caseInfos) {
                if (caseInfo.getAcceptOrg().equals(acceptOrg)) {
                    filteredProducts.add(caseInfo);
                }
            }
            return filteredProducts;
        }else {
            return null;
        }
    }

    public static List<CaseInfo> filterCaseByCasePrg(List<CaseInfo> caseInfos, String casePrg) {
        List<CaseInfo> filteredProducts = new ArrayList<>();
        if(caseInfos != null && !caseInfos.isEmpty()) {
            for (CaseInfo caseInfo : caseInfos) {
                if (caseInfo.getCasePrg().equals(casePrg)) {
                    filteredProducts.add(caseInfo);
                }
            }
            return filteredProducts;
        }else {
            return null;
        }

    }

    public static List<CaseInfo> filterCaseByCaseBegin(List<CaseInfo> caseInfos, LocalDate caseBegin, LocalDate caseEnd) {
        List<CaseInfo> filteredProducts = new ArrayList<>();
        if(caseInfos != null && !caseInfos.isEmpty()) {
            for (CaseInfo caseInfo : caseInfos) {
                if (caseEnd.compareTo(caseInfo.getCaseBegin()) >= 0 && caseBegin.compareTo(caseInfo.getCaseBegin()) <= 0) {
                    filteredProducts.add(caseInfo);
                }
            }
            return filteredProducts;
        }else {
            return null;
        }

    }
}

