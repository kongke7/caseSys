package com.example.casesys.utils;
import com.example.casesys.dto.CaseInfoDTO;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ExportExcel {

    public static void export(List<CaseInfoDTO> caseInfoDTOList, HttpServletResponse response) throws IOException {
        // 创建excel对象
        HSSFWorkbook wk = new HSSFWorkbook();
        // 创建一张案件信息表
        HSSFSheet sheet = wk.createSheet("案件信息表");
        // 创建标题行
        HSSFRow smallTitle = sheet.createRow(0);
        smallTitle.createCell(0).setCellValue("序号");
        smallTitle.createCell(1).setCellValue("案件名称");
        smallTitle.createCell(2).setCellValue("案件编号");
        smallTitle.createCell(3).setCellValue("立案时间");
        smallTitle.createCell(4).setCellValue("受案单位");
        smallTitle.createCell(5).setCellValue("办理人员");
        smallTitle.createCell(6).setCellValue("嫌犯姓名");
        smallTitle.createCell(7).setCellValue("案发地区");
        smallTitle.createCell(8).setCellValue("简要案情");
        smallTitle.createCell(9).setCellValue("涉案罪名");
        smallTitle.createCell(10).setCellValue("有无前科");
        smallTitle.createCell(11).setCellValue("采取强制措施时间");
        smallTitle.createCell(12).setCellValue("采取强制措施种类");
        smallTitle.createCell(13).setCellValue("采取强制措施依据");
        smallTitle.createCell(14).setCellValue("变更强制措施时间");
        smallTitle.createCell(15).setCellValue("变更强制措施种类");
        smallTitle.createCell(16).setCellValue("变更强制措施依据");
        smallTitle.createCell(17).setCellValue("案件结束时间");
        smallTitle.createCell(18).setCellValue("办案进度");
        smallTitle.createCell(19).setCellValue("用户id");
        smallTitle.createCell(20).setCellValue("变更强制措施时间");
        smallTitle.createCell(21).setCellValue("变更强制措施类型");
        smallTitle.createCell(22).setCellValue("变更强制措施时间");
        smallTitle.createCell(23).setCellValue("变更强制措施巷类型");
        int count = 1;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        for (CaseInfoDTO caseInfoDTO : caseInfoDTOList) {
            HSSFRow row = sheet.createRow(count++);
            row.createCell(0).setCellValue(caseInfoDTO.getId());
            row.createCell(1).setCellValue(caseInfoDTO.getCaseName());
            row.createCell(2).setCellValue(caseInfoDTO.getCaseId());
            if(caseInfoDTO.getCaseBegin() != null) {
                row.createCell(3).setCellValue(caseInfoDTO.getCaseBegin().format(formatter));
            }
            row.createCell(4).setCellValue(caseInfoDTO.getAcceptOrg());
            row.createCell(5).setCellValue(caseInfoDTO.getAcceptStaff());
            row.createCell(6).setCellValue(caseInfoDTO.getSuspectsName());
            row.createCell(7).setCellValue(caseInfoDTO.getCaseArea());
            row.createCell(8).setCellValue(caseInfoDTO.getCaseBrief());
            row.createCell(9).setCellValue(caseInfoDTO.getCaseCharges());
            row.createCell(10).setCellValue(caseInfoDTO.getRecord());
            if(caseInfoDTO.getTkCoerciveTime() != null) {
                row.createCell(11).setCellValue(caseInfoDTO.getTkCoerciveTime().format(formatter));
            }
            row.createCell(12).setCellValue(caseInfoDTO.getTkCoerciveType());
            row.createCell(13).setCellValue(caseInfoDTO.getTkCoerciveBasis());
            if(caseInfoDTO.getCgCoerciveTime() != null) {
                row.createCell(14).setCellValue(caseInfoDTO.getCgCoerciveTime().format(formatter));
            }
            row.createCell(15).setCellValue(caseInfoDTO.getCgCoerciveType());
            row.createCell(16).setCellValue(caseInfoDTO.getCgCoerciveBasis());
            if(caseInfoDTO.getDeadlineWarn() != null) {
                row.createCell(17).setCellValue(caseInfoDTO.getDeadlineWarn().format(formatter));
            }
            row.createCell(18).setCellValue(caseInfoDTO.getCasePrg());
            row.createCell(19).setCellValue(caseInfoDTO.getUserNumber());
            if(caseInfoDTO.getCgCoerciveTimes() != null) {
                row.createCell(20).setCellValue(caseInfoDTO.getCgCoerciveTimes().format(formatter));
            }
            row.createCell(21).setCellValue(caseInfoDTO.getCgCoerciveStyle());
            if(caseInfoDTO.getCgCoerciveTiming() != null) {
                row.createCell(22).setCellValue(caseInfoDTO.getCgCoerciveTiming().format(formatter));
            }
            row.createCell(23).setCellValue(caseInfoDTO.getCgCoerciveGenre());
//            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
//            response.setHeader("Content-Disposition", "attachment; filename=caseInfo.xlsx");
            // 将工作簿写入响应流
        }
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//        wk.write(outputStream);
//        wk.close();
//        byte[] fileContent = outputStream.toByteArray();
//        // 设置响应头
//        HttpHeaders httpHeaders = new HttpHeaders();
//        httpHeaders.setContentType(MediaType.valueOf("application/vnd.ms-excel"));
//        httpHeaders.setContentDispositionFormData("attachment", "case.xlsx");
//        httpHeaders.set(HttpHeaders.CONTENT_ENCODING, "UTF-8");
//        return new ResponseEntity<>(fileContent, httpHeaders, HttpStatus.OK);
        OutputStream output;
        try {
            output = response.getOutputStream();
            //清空缓存
            response.reset();
            //定义浏览器响应表头，顺带定义下载名，比如students(中文名需要转义)
            String s = "案件信息表" + System.currentTimeMillis() + ".xls";
            String fileName = URLEncoder.encode(s, "UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName);
            //定义下载的类型，标明是excel文件
            response.setContentType("application/vnd.ms-excel");
//            response.setCharacterEncoding("UTF-8");
            //这时候把创建好的excel写入到输出流
            wk.write(output);
            //养成好习惯，出门记得随手关门
            output.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

//    public static void toExcel(HSSFWorkbook wk) throws Exception{
//        OutputStream outputStream = new FileOutputStream(new File("d:\\case.xls"));
//        wk.write(outputStream);
//        wk.close();
//    }
}
