package com.example.casesys.utils;

import cn.hutool.core.util.StrUtil;

public class RegexUtils {

    /**
     * 判断用户账号是否符合标准
     * @param phone
     * @return
     */
    public static boolean isNumberInvalid(String phone){
        return mismatch(phone, RegexPatterns.NUMBER_REGEX);
    }

    /**
     * 判断身份证号是否符合规范
     * @param is_number
     * @return
     */
    public static boolean isIdNumber(String is_number){
        return mismatch(is_number, RegexPatterns.ID_NUMBER);
    }

    /**
     * 判断密码是否符合规范
     * @param password
     * @return
     */
    public static boolean isPassword(String password){
        return mismatch(password, RegexPatterns.PASSWORD);
    }

    // 校验是否不符合正则格式
    private static boolean mismatch(String str, String regex){
        if (StrUtil.isBlank(str)) {
            return true;
        }
        return !str.matches(regex);
    }
}
