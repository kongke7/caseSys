package com.example.casesys.utils;
import com.example.casesys.dto.UserDTO;
import javax.servlet.http.HttpSession;

/**
 * 获取当前用户信息
 * @author kongke
 * @date 2023/09/13
 */
public class UserHolder {

    public static void saveUser(UserDTO user , HttpSession session){
        session.setMaxInactiveInterval(3600);
        session.setAttribute("user",user);
    }

    public static UserDTO getUser(HttpSession session){
        return (UserDTO) session.getAttribute("user");
    }

    public static void removeUser(HttpSession session){
        session.removeAttribute("user");
    }
}
