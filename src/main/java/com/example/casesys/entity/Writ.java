package com.example.casesys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 文书
 * @TableName tb_writ
 */
@TableName(value ="tb_writ")
@Data
public class Writ implements Serializable {
    /**
     * 文书id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 服务器中的名称
     */
    private String writNameSer;

    /**
     * 文件客户端名称
     */
    private String writNameCli;

    /**
     * 上传者的id，外键
     */
    private Integer userId;

    /**
     * 文件类型
     */
    private String writType;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Writ other = (Writ) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getWritNameSer() == null ? other.getWritNameSer() == null : this.getWritNameSer().equals(other.getWritNameSer()))
            && (this.getWritNameCli() == null ? other.getWritNameCli() == null : this.getWritNameCli().equals(other.getWritNameCli()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getWritNameSer() == null) ? 0 : getWritNameSer().hashCode());
        result = prime * result + ((getWritNameCli() == null) ? 0 : getWritNameCli().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", writNameSer=").append(writNameSer);
        sb.append(", writNameCli=").append(writNameCli);
        sb.append(", userId=").append(userId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}