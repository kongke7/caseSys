package com.example.casesys.entity;
import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 案件信息表
 * </p>
 *
 * @author hongren
 * @since 2023-09-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_case_info")
public class CaseInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 序号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 案件名称
     */
    private String caseName;

    /**
     * 案件编号
     */
    private String caseId;

    /**
     * 立案时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate caseBegin;

    /**
     * 受案单位
     */
    private String acceptOrg;

    /**
     * 办理人员
     */
    private String acceptStaff;

    /**
     * 嫌犯姓名
     */
    private String suspectsName;

    /**
     * 嫌犯性别
     */
    private String suspectsSex;

    /**
     * 嫌犯身份证号
     */
    private String suspectsId;

    /**
     * 案发地区
     */
    private String caseArea;

    /**
     * 简要案情
     */
    private String caseBrief;

    /**
     * 涉案罪名
     */
    private String caseCharges;

    /**
     * 有无前科
     */
    private Integer record;

    /**
     * 采取强制措施时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tkCoerciveTime;

    /**
     * 采取强制措施种类
     */
    private String tkCoerciveType;

    /**
     * 采取强制措施依据
     */
    private String tkCoerciveBasis;

    /**
     * 变更强制措施时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate cgCoerciveTime;

    /**
     * 变更强制措施种类
     */
    private String cgCoerciveType;

    /**
     * 变更强制措施依据
     */
    private String cgCoerciveBasis;

    /**
     * 时间期限预警
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deadlineWarn;

    /**
     * 办案进度
     */
    private String casePrg;

    /**
     * 案件审查意见
     */
    private String caseOp;

    /**
     * 备注
     */
    private String note;

    /**
     * 用户id
     */
    private Integer userNumber;


    @TableField(exist = false)
    private Long nearExp;

    /**
     * 逻辑删除
     */
    @TableLogic
    private Integer isDelete;

    /**
     * 变更强制措施时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate cgCoerciveTimes;

    /**
     * 变更强制措施类型
     */
    private String cgCoerciveStyle;

    /**
     * 变更强制措施时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate cgCoerciveTiming;

    /**
     * 变更强制措施巷类型
     */
    private String cgCoerciveGenre;

}
