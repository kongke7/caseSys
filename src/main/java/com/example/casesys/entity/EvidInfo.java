package com.example.casesys.entity;
import com.baomidou.mybatisplus.annotation.*;
import java.time.LocalDate;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 证据信息表
 * </p>
 *
 * @author hongren
 * @since 2023-09-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_evid_info")
public class EvidInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 序号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 案件编号
     */
    private String caseId;

    /**
     * 证据类型
     */
    private String evidType;

    /**
     * 上传时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDate uploadTime;

    /**
     * 服务端存放文件名
     */
    private String fileNameSer;

    /**
     * 客户端展示的文件名
     */
    private String fileNameCli;

    /**
     * 文本
     */
    private String note;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate updateTime;

}
