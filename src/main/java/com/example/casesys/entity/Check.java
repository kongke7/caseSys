package com.example.casesys.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p>
 * 审核信息表
 * </p>
 *
 * @author hongren
 * @since 2023-09-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_check")
public class Check implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 审查id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 主办民警工作目标、计划
     */
    private String checkTarget;

    /**
     * 审结期限
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate checkTerm;

    /**
     * 大队负责人意见
     */
    private String responsibleOpinion;

    /**
     * 支队领导意见
     */
    private String leaderOpinion;

    /**
     * 案件id
     */
    private Integer caseId;

    /**
     * 案件名称
     */
    @TableField(exist = false)
    private String caseName;
}
