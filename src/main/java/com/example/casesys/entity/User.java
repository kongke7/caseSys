package com.example.casesys.entity;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author hongren
 * @since 2023-09-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 账号
     */
    private String accounts;

    /**
     * 密码
     */
    private String passwords;

    /**
     * 使用人
     */
    private String userName;

    /**
     * 所属部门
     */
    private String department;

    /**
     * 身份证号
     */
    @TableField("user_Id")
    private String userId;

    /**
     * 警号
     */
    private String policeNum;

    /**
     * 账号类型：0用户、1管理员、2超管
     */
    private Integer accType;

    /**
     * token值
     */
    @TableField(exist = false)
    private String token;

    /**
     * 逻辑删除
     */
    @TableLogic
    private Integer isDelete;

    /**
     * 新密码
     */
    @TableField(exist = false)
    private String newPassword;


}
