package com.example.casesys.service;
import com.example.casesys.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.casesys.vo.Result;
import javax.servlet.http.HttpSession;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author hongren
 * @since 2023-09-11
 */
public interface UserService extends IService<User> {

    Result register(User user );

    Result login(User user, HttpSession session);

    Result updateType(int id);

    Result queryAllUser(Integer currentPage, HttpSession session);

    Result deleteById(Integer id);

    Result updatePasswordBySelf(User user, HttpSession session);

    Result updateUser(User user);

    Result selectUser(Integer currentPage, HttpSession session);

    Result updateByAdministrator(User user);

    Result deleteByAdministrator(Integer id);

    Result removeAdministrator(Integer id);

    Result selectSelf(HttpSession session);
}
