package com.example.casesys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.casesys.dto.UserDTO;
import com.example.casesys.entity.CaseInfo;
import com.example.casesys.entity.Check;
import com.example.casesys.entity.User;
import com.example.casesys.mapper.CaseInfoMapper;
import com.example.casesys.mapper.CheckMapper;
import com.example.casesys.mapper.UserMapper;
import com.example.casesys.service.CheckService;
import com.example.casesys.utils.UserHolder;
import com.example.casesys.vo.Result;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

@Service
public class CheckServiceImpl extends ServiceImpl<CheckMapper, Check> implements CheckService {

    @Resource
    private CheckMapper checkMapper;

    @Resource
    private CaseInfoMapper caseInfoMapper;

    @Resource
    private UserMapper userMapper;
    /**
     * 增加审查信息
     * @param check
     * @param session
     * @return
     */
    @Override
    public Result addCheck(Check check, HttpSession session) {
        UserDTO userDTO = UserHolder.getUser(session);
        Integer accType = userDTO.getAccType();
        Integer caseId = check.getCaseId();
        QueryWrapper<Check> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("case_id", caseId);
        Check check2 = checkMapper.selectOne(queryWrapper);
        if(check2 != null){
            return Result.fail().message("请勿重复添加！");
        }
        if(accType == 0){
            Check check1 = new Check();
            check1.setCaseId(caseId);
            check1.setCheckTarget(check.getCheckTarget());
            check1.setCheckTerm(check.getCheckTerm());
            checkMapper.insert(check1);
            Result.ok().message("添加成功");
        }
        CaseInfo caseInfo = caseInfoMapper.selectById(caseId);
        User user = userMapper.selectById(caseInfo.getUserNumber());
        if((accType == 1 || accType == 2) && (userDTO.getUserName().equals(user.getUserName()))) {
            checkMapper.insert(check);
            return Result.ok().message("添加成功");
        } else {
                return Result.fail().message("不是您的案件您无权添加！");
            }

    }

    /**
     * 查询审查信息
     * @param caseId
     * @return
     */
    @Override
    public Result selectCheck(Integer caseId) {
        CaseInfo caseInfo = caseInfoMapper.selectById(caseId);
        if(caseInfo == null){
            return Result.fail().message("案件信息不存在！");
        }
        String caseName = caseInfo.getCaseName();
        QueryWrapper<Check> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("case_id",caseId);
        Check check = checkMapper.selectOne(queryWrapper);
        if(check == null){
            return Result.fail().message("审查信息不存在！");
        }
        check.setCaseName(caseName);
        return Result.ok(check);
    }

    /**
     * 修改审查信息
     * @param session
     * @return
     */
    @Override
    public Result updateCheck(Check check, HttpSession session) {
        UserDTO userDTO = UserHolder.getUser(session);
        Check check1 = checkMapper.selectById(check.getId());
        if(check1 == null){
            return Result.fail().message("审查信息不存在!");
        }
        Integer accType = userDTO.getAccType();
        if(accType == 0){
            Check check2 = new Check();
            check2.setId(check.getId());
            check2.setCheckTarget(check.getCheckTarget());
            check2.setCheckTerm(check.getCheckTerm());
            checkMapper.updateById(check2);
            return Result.ok().message("修改成功");
        }
        CaseInfo caseInfo = caseInfoMapper.selectById(check1.getCaseId());
        //User user = userMapper.selectById(caseInfo.getUserNumber());
        if((accType == 2 || accType == 1) && (userDTO.getUserName().equals(caseInfo.getAcceptStaff()))){
            checkMapper.updateById(check);
            return Result.ok().message("修改成功");
        }
        else {
            Check check3 = new Check();
            check3.setId(check.getId());
            check3.setLeaderOpinion(check.getLeaderOpinion());
            check3.setResponsibleOpinion(check.getResponsibleOpinion());
            checkMapper.updateById(check3);
            return Result.ok().message("修改成功");
        }
    }


}
