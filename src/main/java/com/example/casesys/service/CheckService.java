package com.example.casesys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.casesys.entity.Check;
import com.example.casesys.vo.Result;

import javax.servlet.http.HttpSession;

/**
 * <p>
 * 审核表 服务类
 * </p>
 *
 * @author hongren
 * @since 2023-09-11
 */
public interface CheckService extends IService<Check> {
    Result addCheck(Check check, HttpSession session);

    Result selectCheck(Integer caseId);

    Result updateCheck(Check check, HttpSession session);
}
