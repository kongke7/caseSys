package com.example.casesys.service;

import com.example.casesys.dto.CaseRequestParams;
import com.example.casesys.entity.CaseInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.casesys.vo.Result;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * <p>
 * 案件信息表 服务类
 * </p>
 *
 * @author hongren
 * @since 2023-09-11
 */
public interface CaseInfoService extends IService<CaseInfo> {

    Result selectCase(Integer current , CaseRequestParams params, HttpSession session);

    Result addCase(CaseInfo caseInfo, HttpSession session);

    Result updateCase(CaseInfo caseInfo);

    Result countCase();

    Result deleteCase(Long caseId);

    Result selectNearExpCase(HttpSession httpSession);

    void exportCase(CaseRequestParams params, HttpSession sessio, HttpServletResponse response);

    Result selectCaseInfo(Integer currentPage, HttpSession session);
}
