package com.example.casesys.service;

import com.example.casesys.entity.Writ;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.casesys.vo.Result;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;

/**
* @author kongke
* @description 针对表【tb_writ(文书)】的数据库操作Service
* @createDate 2023-10-13 20:05:46
*/
public interface WritService extends IService<Writ> {


    Result upload(String type ,MultipartFile file , HttpSession session);

    ResponseEntity<byte[]> download(String serName);

    Result delWrit(Integer writId , HttpSession session);

    Result getWrits(Integer current , String writType);
}
