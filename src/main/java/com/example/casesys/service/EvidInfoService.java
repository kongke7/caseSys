package com.example.casesys.service;
import com.example.casesys.entity.EvidInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.casesys.vo.Result;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 证据信息表 服务类
 * </p>
 *
 * @author hongren
 * @since 2023-09-11
 */
public interface EvidInfoService extends IService<EvidInfo> {

    Result selectEvid(String caseId , Integer pageId);

    Result addEvid(EvidInfo evidInfo);

    Result updateEvid(EvidInfo evidInfo);

    Result delEvid(Integer id);

    Result uploadFile(MultipartFile file);

    Result updateFile(Integer id, MultipartFile file);

    ResponseEntity<byte[]> downloadFile(String fileName);
}
