package com.example.casesys.config;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
@EnableSwaggerBootstrapUI
public class SwaggerConfig {

    @Bean
    public Docket createRestApi() {
        List<Parameter> parameterList = new ArrayList<>();
        ParameterBuilder parameterBuilder = new ParameterBuilder();
        parameterBuilder.name("Authorization").description("请求头token凭证").modelRef(new ModelRef("String"))
                .parameterType("header").required(false);
        parameterList.add(parameterBuilder.build());
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("v1")
                .apiInfo(apiInfo())
                .select()
                //控制器位置
                .apis(RequestHandlerSelectors.basePackage("com.example.casesys.controller"))
                .paths(PathSelectors.any())  //表示扫描所有路径
                .build()
                .globalOperationParameters(parameterList);
    }


    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("项目管理接口文档")    //文档的标题，可自定义
                .description("接口文档")     //测试的接口文档，可自定义
                .contact(new Contact("","http://43.139.114.27:9110/doc.html","2160923185@qq.com"))   //联系人信息，可自定义
                .version("1.0")   //版本信息，可自定义
                .build();
    }




}
