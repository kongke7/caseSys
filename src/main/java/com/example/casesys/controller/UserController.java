package com.example.casesys.controller;
import com.example.casesys.entity.User;
import com.example.casesys.service.UserService;
import com.example.casesys.vo.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 *
 * @author hongren
 * @since 2023-09-11
 */
@CrossOrigin //跨域
@RestController
@RequestMapping("/user")
@Api(tags = "用户信息管理接口")
public class UserController {
    @Resource
    private UserService userService;

    @ApiOperation("用户注册接口")
    @PostMapping("/register")
    public Result register(@RequestBody User user){
        return userService.register(user);
    }

    @ApiOperation("用户登录接口")
    @PostMapping("/login")
    public Result login(@RequestBody User user , HttpSession session){
        return userService.login(user , session);
    }

    @ApiOperation("用户修改密码")
    @PutMapping("/updatePassword")
    public Result updatePassword(@RequestBody User user, HttpSession session){
        return userService.updatePasswordBySelf(user, session);
    }

    @ApiOperation("超级管理员任命管理员接口")
    @PutMapping("/updateAccType/{id}")
    public Result updateAccType(@PathVariable int id){
        return userService.updateType(id);
    }


    @ApiOperation("超级管理员查询所有用户信息接口")
    @GetMapping("/queryAllUser/{currentPage}")
    public Result queryAllUser(@PathVariable Integer currentPage, HttpSession session){
        return userService.queryAllUser(currentPage, session);
    }

    @ApiOperation("超级管理员删除用户信息接口")
    @DeleteMapping("/deleteUser/{id}")
    public Result deleteUserById(@PathVariable Integer id) {
        return userService.deleteById(id);
    }

    @ApiOperation("超级管理员修改用户信息接口")
    @PutMapping("/updateUser")
    public Result updateUser(@RequestBody User user){
        return userService.updateUser(user);
    }

    @ApiOperation("管理员查询本单位用户信息接口")
    @GetMapping("/selectUser/{currentPage}")
    public Result selectUser(@PathVariable Integer currentPage, HttpSession session){
        return userService.selectUser(currentPage , session);
    }

    @ApiOperation("管理员修改本单位用户信息接口")
    @PutMapping("/updateByAdministrator")
    public Result updateByAdministrator(@RequestBody User user){
        return userService.updateByAdministrator(user);
    }

    @ApiOperation("管理员删除本单位用户信息")
    @DeleteMapping("/deleteByAdministrator/{id}")
    public Result deleteByAdministrator(@PathVariable Integer id){
        return userService.deleteByAdministrator(id);
    }

    @ApiOperation("超级管理员去除管理员权限")
    @PutMapping("/removeAdministrator/{id}")
    public Result removeAdministrator(@PathVariable Integer id){
        return userService.removeAdministrator(id);
    }

    @ApiOperation("用户查看个人信息接口")
    @GetMapping("/selectSelf")
    public Result selectSelf(HttpSession session){
        return userService.selectSelf(session);
    }


}

