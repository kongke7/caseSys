package com.example.casesys.controller;

import com.example.casesys.entity.EvidInfo;
import com.example.casesys.service.EvidInfoService;
import com.example.casesys.vo.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author hongren
 * @since 2023-09-11
 */
@CrossOrigin //跨域
@RestController
@RequestMapping("/evid-info")
@Api(tags = "证据信息管理接口")
public class EvidInfoController {

    @Resource
    private EvidInfoService evidInfoService;

    /**
     * 查询证据
     *
     * @param caseId 案件id
     * @return {@link Result}
     */
    @GetMapping("/getEvid")
    public Result selectEvid(@RequestParam("caseId") String caseId, @RequestParam("pageId") Integer pageId) {
        return evidInfoService.selectEvid(caseId, pageId);
    }


    /**
     * 增加证据
     *
     * @return {@link Result}
     */
    @PostMapping("/addEvid")
    public Result addEvid(@RequestBody EvidInfo evidInfo) {
        return evidInfoService.addEvid(evidInfo);
    }

    /**
     * 更新证据
     *
     * @return {@link Result}
     */
    @PutMapping("/updateEvid")
    public Result updateEvid(@RequestBody EvidInfo evidInfo) {
        return evidInfoService.updateEvid(evidInfo);
    }

    /**
     * 删除证据
     *
     * @return {@link Result}
     */
    @DeleteMapping("/delEvid")
    public Result delEvid(@RequestParam("evidId") Integer id) {
        return evidInfoService.delEvid(id);
    }

    /**
     * 上传证据图片
     * @return
     */
    @ApiOperation("上传证据文件接口")
    @PostMapping("/uploadFile")
    public Result uploadFile(@RequestParam("file") MultipartFile file) {
        return evidInfoService.uploadFile(file);
    }

    /**
     * 下载文件
     *
     * @return {@link Result}
     */
    @ApiOperation(value = "下载证据文件接口",produces = "application/octet-stream")
    @GetMapping("/downloadFile")
    public ResponseEntity<byte[]> downloadFile(@RequestParam("fileName") String fileName) {
        return evidInfoService.downloadFile(fileName);
    }

    /**
     * 文件（删除）
     * @param id
     * @param file
     * @return
     */
    @ApiOperation("修改证据文件接口")
    @PutMapping("/updateFile/{id}")
    public Result updateFile(@PathVariable Integer id,@RequestParam("file") MultipartFile file) {
        return evidInfoService.updateFile(id, file);
    }

}

