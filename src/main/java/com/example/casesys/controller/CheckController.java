package com.example.casesys.controller;
import com.example.casesys.entity.Check;
import com.example.casesys.service.CheckService;
import com.example.casesys.vo.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 *
 * @author hongren
 * @since 2023-09-11
 */
@CrossOrigin //跨域
@RestController
@RequestMapping("/check")
@Api(tags = "审核意见接口")
public class CheckController {

    @Resource
    private CheckService checkService;

    @ApiOperation("增加审核意见信息")
    @PostMapping("/addCheck")
    public Result addCheck(@RequestBody Check check,
                           HttpSession session){
        return checkService.addCheck(check, session);
    }

    /**
     * 查询审核意见信息
     * @param caseId 案件id
     * @return Check
     */
    @ApiOperation("查询审核意见信息")
    @GetMapping("/selectCheck/{caseId}")
    public Result selectCheck(@PathVariable Integer caseId){
        return checkService.selectCheck(caseId);
    }

    @ApiOperation("修改审查意见信息")
    @PutMapping("/updateCheck/{caseId}")
    public Result updateCheck(@RequestBody Check check,
                              HttpSession session){
        return checkService.updateCheck(check, session);
    }

}
