package com.example.casesys.controller;

import com.example.casesys.service.WritService;
import com.example.casesys.vo.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;


/**
 * @author kongke
 */
@CrossOrigin
@RequestMapping("/writ")
@RestController
@Api(tags = "文书管理接口")
public class WritController {

    @Resource
    private WritService writService;


    /**
     * 文书查询
     * @return {@link Result}
     */
    @ApiOperation("文书查询接口")
    @GetMapping("/getWrit")
    public Result getWrits(@RequestParam("current") Integer current , @RequestParam("writType") String writType){
        return writService.getWrits(current ,writType);
    }


    /**
     * 文书上传
     * @return {@link Result}
     */
    @ApiOperation("文书上传接口")
    @PostMapping("/upload")
    public Result uploadWrit(@RequestParam("type") String type, @RequestParam("writ") MultipartFile file  , HttpSession session){
        return writService.upload(type ,file , session);
    }

    /**
     * 文书下载
     * @param serName 服务端存的文件名
     * @return {@link ResponseEntity}<{@link byte[]}>
     */
    @ApiOperation("文书下载接口")
    @GetMapping("/download")
    public ResponseEntity<byte[]> downloadWrit(@RequestParam("serName") String serName){
        return writService.download(serName);
    }

    /**
     * 文书删除
     * @return {@link Result}
     */
    @ApiOperation("文书删除接口")
    @DeleteMapping("/del")
    public Result delWrit(@RequestParam("writId") Integer writId , HttpSession session){
        return writService.delWrit(writId , session);
    }


}
