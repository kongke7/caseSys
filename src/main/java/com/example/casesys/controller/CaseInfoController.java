package com.example.casesys.controller;

import com.example.casesys.dto.CaseRequestParams;
import com.example.casesys.entity.CaseInfo;
import com.example.casesys.service.CaseInfoService;
import com.example.casesys.vo.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author hongren
 * @since 2023-09-11
 */
@CrossOrigin //跨域
@RestController
@RequestMapping("/case-info")
@Api(tags = "案件信息管理接口")
public class CaseInfoController {

    @Resource
    private CaseInfoService caseInfoService;

    /**
     * 查询案件
     *
     * @param current 当前页码
     * @return {@link Result}
     */
    @ApiOperation("案件查询和条件筛选功能接口")
    @PostMapping("/getCase/{current}")
    public Result getCaseInfo(@PathVariable Integer current,
                              @RequestBody CaseRequestParams params,
                              HttpSession session) {
        return caseInfoService.selectCase(current, params, session);
    }

    /**
     * 添加案件
     *
     * @param caseInfo 案件对象
     * @return {@link Result}
     */
    @PostMapping("/addCase")
    public Result addCase(@RequestBody CaseInfo caseInfo, HttpSession session) {
        return caseInfoService.addCase(caseInfo, session);
    }


    /**
     * 更新案件
     *
     * @return {@link Result}
     */
    @PutMapping("/updateCase")
    public Result updateCase(@RequestBody CaseInfo caseInfo) {
        return caseInfoService.updateCase(caseInfo);
    }


    /**
     * 统计案件
     *
     * @return {@link Result}
     */
    @GetMapping("/countCase")
    public Result countCase() {
        return caseInfoService.countCase();
    }

    /**
     * 逻辑删除
     *
     * @return {@link Result}
     */
    @DeleteMapping("/delCase")
    public Result delCase(@RequestParam("caseId") Long caseId) {
        return caseInfoService.deleteCase(caseId);
    }


    @ApiOperation("判断案件是否过期")
    @GetMapping("/getNearExpCase")
    public Result selectNearExpCase(HttpSession session) {
        return caseInfoService.selectNearExpCase(session);
    }


    @ApiOperation("案件数据导出接口")
    @PostMapping(value = "/exportCase", produces = "application/octet-stream")
    public void exportCase(@RequestBody CaseRequestParams params,
                           HttpSession session,
                           HttpServletResponse response) {
        caseInfoService.exportCase(params, session, response);
    }

    @ApiOperation("管理员获取最近一周的案件信息")
    @GetMapping("/selectCase/{currentPage}")
    public Result selectCase(@PathVariable Integer currentPage,
                             HttpSession session) {
        return caseInfoService.selectCaseInfo(currentPage, session);
    }


}

