package com.example.casesys.dto;

import com.example.casesys.entity.CaseInfo;
import lombok.Data;
import java.util.List;

/**
 * @author kongke
 */
@Data
public class CaseDTO {

   /**
    * 案件信息
    */
   private List<CaseInfo> caseList;

   /**
    * 案件统计
    */
   private Long caseCount;

   /**
    * 涉案罪名（案由）集合
    */
   private List<String> caseChargesList;

   /**
    * 办案单位集合
    */
   private List<String> acceptOrgList;

   /**
    * 案件办理阶段
    */
   private List<String> casePrgList;
}
