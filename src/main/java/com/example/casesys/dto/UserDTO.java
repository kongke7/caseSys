package com.example.casesys.dto;


import lombok.Data;

/**
 * @author kongke
 */
@Data
public class UserDTO {

    /**
     * 用户id
     */
    private Integer id;

    /**
     * 账号
     */
    private String accounts;

    /**
     * 使用人
     */
    private String userName;

    /**
     * 所属部门
     */
    private String department;

    /**
     * 警号
     */
    private String policeNum;

    /**
     * 账号类型：用户、管理员、超管
     */
    private Integer accType;

    /**
     * token值
     */
    private String token;


}
