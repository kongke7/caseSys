package com.example.casesys.dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class CaseRequestParams {

    /**
     * 涉案罪名(案由)
     */
    private String caseCharges;

    /**
     * 起始时间
     */
    private LocalDate caseBegin;

    /**
     * 截止时间
     */
    private LocalDate caseEnd;

    /**
     * 受案单位
     */
    private String acceptOrg;

    /**
     * 办案进度
     */
    private String casePrg;

    public boolean isEmpty(){
        return (caseCharges == null && caseBegin == null && caseEnd == null && acceptOrg == null && casePrg == null);
    }
}
