package com.example.casesys.dto;
import lombok.Data;

import java.util.List;
@Data
public class UserDTOPlus {

    private List<UserDTO> userDTOList;

    /**
     * 总用户条数
     */
    private Long userCount;
}
