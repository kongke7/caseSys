package com.example.casesys.dto;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDate;
@Data
public class CaseInfoDTO {

    /**
     * 序号
     */
    private Long id;

    /**
     * 案件名称
     */
    private String caseName;

    /**
     * 案件编号
     */
    private String caseId;

    /**
     * 立案时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDate caseBegin;

    /**
     * 受案单位
     */
    private String acceptOrg;

    /**
     * 办理人员
     */
    private String acceptStaff;

    /**
     * 嫌犯姓名
     */
    private String suspectsName;

    /**
     * 案发地区
     */
    private String caseArea;

    /**
     * 简要案情
     */
    private String caseBrief;

    /**
     * 涉案罪名
     */
    private String caseCharges;

    /**
     * 有无前科
     */
    private Integer record;

    /**
     * 采取强制措施时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDate tkCoerciveTime;

    /**
     * 采取强制措施种类
     */
    private String tkCoerciveType;

    /**
     * 采取强制措施依据
     */
    private String tkCoerciveBasis;

    /**
     * 变更强制措施时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDate cgCoerciveTime;

    /**
     * 变更强制措施种类
     */
    private String cgCoerciveType;

    /**
     * 变更强制措施依据
     */
    private String cgCoerciveBasis;

    /**
     * 时间期限预警
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDate deadlineWarn;

    /**
     * 办案进度
     */
    private String casePrg;


    /**
     * 用户id
     */
    private Integer userNumber;

    /**
     * 变更强制措施时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDate cgCoerciveTimes;

    /**
     * 变更强制措施类型
     */
    private String cgCoerciveStyle;

    /**
     * 变更强制措施时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDate cgCoerciveTiming;

    /**
     * 变更强制措施巷类型
     */
    private String cgCoerciveGenre;
}
