package com.example.casesys.dto;

import com.example.casesys.entity.EvidInfo;
import lombok.Data;

import java.util.List;

/**
 * @author kongke
 */
@Data
public class EvidDTO {

    /**
     * 证据信息
     */
    private List<EvidInfo> evidList;


    /**
     * 总条数
     */
    private long count;
}
