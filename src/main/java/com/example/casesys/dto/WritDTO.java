package com.example.casesys.dto;

import com.example.casesys.entity.Writ;
import lombok.Data;

import java.util.List;

/**
 * @author kongke
 */
@Data
public class WritDTO {
    /**
     * 文书信息
     */
    private List<Writ> writs;

    /**
     * 文书总数量
     */
    private Long count;
}
