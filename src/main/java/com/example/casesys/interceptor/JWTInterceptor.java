package com.example.casesys.interceptor;
import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.example.casesys.utils.JwtUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.servlet.HandlerInterceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class JWTInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Map<String,Object> map = new HashMap<>();
        //获取请求头中令牌
        String token = request.getHeader("Authorization");
        try {
            JwtUtil.verify(token);//验证令牌
            return true;
        } catch (SignatureVerificationException e) {
            e.printStackTrace();
            map.put("message","无效签名!");
        } catch (TokenExpiredException e){
            e.printStackTrace();
            map.put("message","token过期!");
        } catch (AlgorithmMismatchException e){
            e.printStackTrace();
            map.put("message","token算法不一致");
        } catch (Exception e){
            e.printStackTrace();
            map.put("message","token无效!");
        }
        map.put("code",202);//设置状态
        //将map转为json
        String json = new ObjectMapper().writeValueAsString(map);
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().println(json);
        return false;
    }


}
