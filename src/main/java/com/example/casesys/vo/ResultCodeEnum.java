package com.example.casesys.vo;

import lombok.Getter;

@Getter
public enum ResultCodeEnum {
    SUCCESS(200,"成功"),
    SAVE_SUCCESS(200,"保存成功"),
    SELECT_SUCCESS(200,"查询成功"),
    UPDATE_SUCCESS(200,"更新成功"),
    DELETE_SUCCESS(200,"删除成功"),
    FAIL(201,"失败"),
    SERVICE_ERROR(2012,"服务异常"),
    DATA_ERROR(204,"数据异常"),
    LOGIN_AUTH(208,"未登入"),
    PERMISSION(209,"没有权限");

    private Integer code;
    private String message;
    private ResultCodeEnum(Integer code, String message){
        this.code = code;
        this.message = message;
    }

}
