package com.example.casesys.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class Result<T> implements Serializable {
    private Integer code; //状态码
    private String message; //返回信息
    private T data; //数据

    //私有化
    private Result(){}

    //封装返回的数据
    public static <T> Result<T> build(T body, ResultCodeEnum resultCodeEnum){
        Result<T> result = new Result<>();
        //封装数据
        if(body != null){
            result.setData(body);
        }
        //状态码
        result.setCode(resultCodeEnum.getCode());
        //相关返回数据
        result.setMessage(resultCodeEnum.getMessage());
        return result;
    }

    //成功
    //成功但里面没有数据
    public static<T> Result<T> ok(){
        return build(null, ResultCodeEnum.SUCCESS);
    }
    //成功里面有数据
    public static <T> Result<T> ok(T data){
        return build(data, ResultCodeEnum.SUCCESS);
    }
    public static<T> Result<T> ok(T data,ResultCodeEnum resultCodeEnum){
        return build(data,resultCodeEnum);
    }

    //失败
    //失败里面没数据
    public static<T> Result<T> fail(){
        return build(null, ResultCodeEnum.FAIL);
    }
    //失败里面有数据
    public static<T> Result<T> fail(T data){
        return build(data, ResultCodeEnum.FAIL);
    }

    public static<T> Result<T> fail(Integer code, String message){
        Result<T> result = new Result<>();
        result.setCode(code);
        result.setMessage(message);
        result.setData(null);
        return  result;
    }
    public static<T> Result<T> fail(T data, ResultCodeEnum dataError) {
        return build(data,dataError);
    }

    public Result<T> message(String msg){
        this.setMessage(msg);
        return this;
    }

    public Result<T> code(Integer code){
        this.setCode(code);
        return this;
    }

}
